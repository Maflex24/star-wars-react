import './App.css';
import CharacterRandomizer from './CharacterRandomizer';

function App() {
  return (
    <div className="App">
      <header className="App-header">
        <CharacterRandomizer />
      </header>
    </div>
  );
}

export default App;
