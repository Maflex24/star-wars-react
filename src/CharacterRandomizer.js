import React from "react";

class CharacterRandomizer extends React.Component {    
    constructor() {
        super()

        this.state = {
            name : null,
            gender : null,
            homeworld : null,
            wiki : null,
            imageURL : null,
            genderGreeting : null,
            isCharacterLoaded : false
        }
    }

    getRandomNumber(min, max) {
        min = Math.ceil(min);
        max = Math.floor(max);
        return Math.floor(Math.random() * (max - min + 1)) + min;
    }

    setGreetings(gender) {
        if (gender === "male") {
            return 'mister '
        }
        else if (gender === "female") {
            return 'lady '
        }
        else return null
    }

    isFrom(word) {
        if (word === undefined) {
            return ''        
        }
        else {
            return "from " + word.charAt(0).toUpperCase() + word.slice(1)
        }
    }

    randomizeCharacter() {
        const charID = this.getRandomNumber(1, 88)
        const url = `https://akabab.github.io/starwars-api/api/id/${charID}.json`
        console.log("characterID:", charID)
        
        fetch(url)
            .then(response => response.json())
            .then(data => {
                this.setState({
                    name : data.name,
                    gender : data.gender,
                    homeworld : data.homeworld,
                    wiki : data.wiki,
                    imageURL : data.image,
                    genderGreeting : this.setGreetings(data.gender),
                    isCharacterLoaded : true
                }, console.log(data), console.log("homeworld:", data.homeworld, typeof(data.homeworld)))
            })

    }

    render() {
        return(
            <div>           
                {
                    this.state.isCharacterLoaded &&
                        <p>
                            <img className="CharacterImage" src={this.state.imageURL} alt={this.state.name} /> <br/>
                            It's the {this.state.genderGreeting}
                            <span className="charName">{this.state.name}</span> <br></br> 
                            {this.isFrom(this.state.homeworld)} <br/> <br/>
                            More information about {this.state.name} here: <br/> <a href={this.state.wiki} target="_blank" rel="noreferrer"> {this.state.name} Wikipedia </a>
                        </p>
                }

                <button 
                    className="GenerateCharacter"
                    onClick={() => this.randomizeCharacter()}
                    > 
                        Randomize Character
                </button>
            </div>
        )
    }
}

export default CharacterRandomizer;